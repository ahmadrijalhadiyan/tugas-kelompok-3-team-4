/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.binus.tugaskelompok3;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class TugasKelompok3 {
    static boolean checkUserAnswer(String answer, String[] correctAnswer) {
        return Arrays.asList(correctAnswer).contains(answer);
    }
    
    public static void main(String[] args) {
        int score = 0;
        String[] question = {
                "dettli", "secaen", "hkrneo"
        };
        String[][] correctAnswer = {
                {"die", "led", "let", "lei", "lie", "lid", "lit", "tie", "deli", "tied", "edit", "idle", "lied", "diet", "title"},
                {"sec", "can", "cane", "scan", "scene", "case", "cease", "tie"},
                {"honk", "honker", "roe", "ore", "her", "hen", "one"},
        };
        
        System.out.println("\t     Coepoe Word Puzzle!"
	                + "\n=======================================================");
                
        System.out.println("\nRules : "
                + "\n 1. Buatlah Sebuah Kata dengan karater, Minimal 3 Karakter & Maksimal 6 Karater"
                + "\n 2. Untuk Setiap Levelnya, Kamu akan memberikan 10 jawaban yang menurutmu benar"
                + "\n 3. Untuk Lanjut ke Level berikutnya, kamu harus mendapatkan score minmal 70 poin tiap levelnya");
        
        Scanner scan = new Scanner(System.in);
        int level = 0;
        while (level < 3) {

            System.out.println("\nLevel" + " " + (level+1));
            System.out.print("\n========================\n");
            System.out.print("\n"+question[level]+"\n");
            System.out.print("\n========================\n");
            
            int tempScore = 0;
            String[] tempAnswer = new String[10];
            int ans = 0;

            while (ans < 10) {
                System.out.print("Your #" + (ans+ 1) + " answer : ");
                String answer = scan.next();
                while (answer.length() > 6 || answer.length() < 3) {
                    if(answer.length() < 3){
                        System.out.println("Length minimum is 3");
                    }else{
                        System.out.println("Length maximum is 6");
                    }
                    System.out.print("Your #" + (ans+ 1) + " answer : ");
                    answer = scan.next();
                }

                if (!checkUserAnswer(answer, tempAnswer)) {
                    if (checkUserAnswer(answer, correctAnswer[level])) {
                        tempScore += 10;
                        System.out.println("Correct! , your score : " + tempScore);
                    }
                    tempAnswer[ans] = answer;
                    ans++;
                } else {
                    System.out.println("You had already type this word before...");
                }
            }

            if (tempScore < 70) {
                System.out.println("You lose! Restart [Y, n] : ");
                char restart = scan.next().charAt(0);
                if (restart == 'Y' || restart == 'y') {
                    level = 0;
                } else {
                    break;
                }
            } else {
                System.out.println("Congrats ! your score on this level : " + tempScore);
                score += tempScore;
                level++;
            }
        }
        if (score >= (70*3)){
            System.out.println("Congrats ! your final score : " + score);
        }
        
        }
    }

